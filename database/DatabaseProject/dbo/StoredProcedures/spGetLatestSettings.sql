﻿CREATE PROCEDURE [dbo].[spGetLatestSettings]
AS
BEGIN
    SELECT TOP 1 [SquareMatrixSize], [NumOfStartingLights]
    FROM dbo.Settings
    ORDER BY [Id] DESC
END