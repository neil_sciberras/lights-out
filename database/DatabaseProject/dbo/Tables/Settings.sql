﻿CREATE TABLE [dbo].[Settings]
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [SquareMatrixSize] INT NULL, 
    [NumOfStartingLights] INT NULL
)
