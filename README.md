# Lights Out

Lights Out is a puzzle game consisting of an _n x n_ grid of lights, where the ultimate goal is to switch off all the lights.

# Documentation

## Running the application
To run the application, both the API and the console app need to be set as start up projects in Visual Studio.

## Dependency injection
The default .Net Core dependency injection mechanism is used. For this, each service is added to an _IServiceCollection_ via an extension method in their respective project (inside `ServiceCollectionExtension.cs`), which is then used by the runtime for the dependency resolution.

## Projects
### Database
* [DatabaseProject](./database/DatabaseProject) - an SQL Server Database Project to manage the `SettingsDB` (SQL scripts can be found in the [dbo](./database/DatabaseProject/dbo) directory)


### API
* [lightsout.dto](./src/lightsout/lightsout.dto) - data transfer objects for `Matrix` and `Settings`

* [lightsout.infrastructure](./src/lightsout/lightsout.infrastructure) - connects and reads from `SettingsDB`

* [lightsout.services](./src/lightsout/lightsout.services) - abstracts the interaction with the infrastructure layer, and adds business logic for random positioning of the initially-lit switches

* [lightsout.api](./src/lightsout/lightsout.api) - an <span>ASP.NET</span> Core Web API project, containing two controllers
    * SettingsController (for retrieving matrix settings from the DB)
    * SetupController (to get the initial matrix)

### Console Application
* [lightsout.entities](./src/lightsout/lightsout.entities) - holds the entity object for `Matrix` together with some extension methods for Tuples

* [lightsout.application](./src/lightsout/lightsout.application) - a console application to drive the game. It calls the API (whose URL is configured via `appsetting.json`), sets up and displays the matrix, and then repetitively prompts the user for commands and checks for winning or exit scenarios (until the user decides to quit)

### Other

* [lightsout.utilities](./src/lightsout/lightsout.utilities) - extensions used as basic validation checks for arguments (e.g. that they're not null, empty, etc.)

### Unit tests

Unit tests can be found in [src/lightsout/tests](./src/lightsout/tests).
<p align="center">
  <img src="./assets/test-coverage.png" />
</p> 

# Testing

1. Entering valid commands
<p align="center">
  <img src="./assets/typical-scenario.png" />
</p>

2. Entering invalid commands
<p align="center">
  <img src="./assets/error-handling.png" />
</p>

3. Winning scenario (achieved by modifying the settings in DB, and also commenting out a piece of code to allow initial amount of lights to be zero)
<p align="center">
  <img src="./assets/winning-scenario.png" />
</p>

## API (from [Swagger](https://localhost:5001/swagger/index.html))

1. Settings
<p align="center">
  <img src="./assets/settings-api.png" />
</p>

1. Setup
<p align="center">
  <img src="./assets/setup-api.png" />
</p>