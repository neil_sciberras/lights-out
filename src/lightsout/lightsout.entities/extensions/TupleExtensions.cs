﻿namespace lightsout.entities.extensions
{
    public static class TupleExtensions
    {
        public static bool Between(this (int, int) tuple, int lowerLimit, int upperLimit)
        {
            return tuple.Item1 >= lowerLimit
                   && tuple.Item2 >= lowerLimit
                   && tuple.Item1 <= upperLimit
                   && tuple.Item2 <= upperLimit;
        }

        public static (int, int) OneUp(this (int, int) position)
        {
            return new(position.Item1 - 1, position.Item2);
        }

        public static (int, int) OneDown(this (int, int) position)
        {
            return new(position.Item1 + 1, position.Item2);
        }

        public static (int, int) OneLeft(this (int, int) position)
        {
            return new(position.Item1, position.Item2 - 1);
        }

        public static (int, int) OneRight(this (int, int) position)
        {
            return new(position.Item1, position.Item2 + 1);
        }
    }
}
