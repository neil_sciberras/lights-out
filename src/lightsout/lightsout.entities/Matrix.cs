﻿using System;
using lightsout.entities.extensions;

namespace lightsout.entities
{
    public class Matrix
    {
        public bool[,] Lights { get; }

        public Matrix(bool[,] lights)
        {
            Lights = lights;
        }

        public int NumOfLitLights()
        {
            var counter = 0;

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (Lights[i, j]) counter++;
                }
            }

            return counter;
        }

        public Matrix ToggleThisAndAdjacent((int, int) position)
        {
            if (!WithinMatrix(position))
                throw new ArgumentOutOfRangeException();

            return Toggle(position)
                .Toggle(position.OneUp())
                .Toggle(position.OneDown())
                .Toggle(position.OneLeft())
                .Toggle(position.OneRight());
        }

        private Matrix Toggle((int, int) position)
        {
            var newLights = (bool[,])Lights.Clone();

            if (WithinMatrix(position))
            {
                newLights[position.Item1, position.Item2] = !newLights[position.Item1, position.Item2];
            }

            return new Matrix(newLights);
        }

        private bool WithinMatrix((int, int) position)
        {
            return position.Between(0, Lights.GetLength(0) - 1);
        }

        public string GetStringRep()
        {
            var stringRepresentation = "\n";

            for (int i = 0; i < Lights.GetLength(0); i++)
            {
                for (int j = 0; j < Lights.GetLength(1); j++)
                {
                    stringRepresentation += Lights[i, j] ? "O\u2551" : "-\u2551";
                }

                stringRepresentation += '\n';
            }

            return stringRepresentation;
        }
    }
}
