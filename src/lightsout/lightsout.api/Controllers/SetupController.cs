﻿using System.Threading.Tasks;

using lightsout.services.interfaces;
using lightsout.utilities;

using Microsoft.AspNetCore.Mvc;

namespace lightsout.api.Controllers
{
    [ApiController]
    [Route("setup")]
    public class SetupController : BaseController
    {
        private readonly ISetupService _setupService;

        public SetupController(ISetupService setupService)
        {
            _setupService = setupService.NotNull(nameof(setupService));
        }

        [HttpGet]
        [Route("initial-matrix")]
        public async Task<IActionResult> GetInitialMatrixAsync([FromQuery] int size, [FromQuery] int numOfStartingLights)
        {
            return await TryExecuteAsync(() => _setupService.GetInitialMatrixAsync(size, numOfStartingLights));
        }
    }
}