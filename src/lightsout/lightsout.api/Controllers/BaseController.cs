﻿using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace lightsout.api.Controllers
{
    public abstract class BaseController
    {
        protected async Task<IActionResult> TryExecuteAsync<T>(Func<Task<T>> action)
        {
            try
            {
                return new OkObjectResult(await action());
            }
            catch
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
