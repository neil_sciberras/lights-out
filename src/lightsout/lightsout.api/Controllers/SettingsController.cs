﻿using System.Threading.Tasks;

using lightsout.services.interfaces;
using lightsout.utilities;

using Microsoft.AspNetCore.Mvc;

namespace lightsout.api.Controllers
{
    [ApiController]
    [Route("settings")]
    public class SettingsController : BaseController
    {
        private readonly ISettingsService _settingsService;

        public SettingsController(ISettingsService settingsService)
        {
            _settingsService = settingsService.NotNull(nameof(settingsService));
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            return await TryExecuteAsync(() => _settingsService.GetSettingsAsync());
        }
    }
}
