﻿using System.Threading.Tasks;

using lightsout.dto;

namespace lightsout.application.clients.interfaces
{
    public interface ISetupClient
    {
        public Task<Matrix> GetInitialMatrixAsync(string url, int size, int numOfStartingLights);
    }
}
