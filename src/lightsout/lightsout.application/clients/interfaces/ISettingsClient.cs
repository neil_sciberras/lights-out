﻿using System.Threading.Tasks;

using lightsout.dto;

namespace lightsout.application.clients.interfaces
{
    public interface ISettingsClient
    {
        public Task<Settings> GetMatrixSettingsAsync(string url);
    }
}
