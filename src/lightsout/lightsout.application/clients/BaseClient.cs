﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace lightsout.application.clients
{
    public abstract class BaseClient
    {
        public async Task<T> GetAsync<T>(string url, string path, string nameOfApi)
        {
            var client = new HttpClient { BaseAddress = new Uri(url) };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync(path);

            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                Console.WriteLine($"Error while setting up matrix. Couldn't reach {nameOfApi}. Aborting..");
                throw new Exception();
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(responseBody);
        }
    }
}
