﻿using System.Threading.Tasks;

using lightsout.application.clients.interfaces;
using lightsout.dto;

namespace lightsout.application.clients
{
    public class SetupClient : BaseClient, ISetupClient
    {
        public Task<Matrix> GetInitialMatrixAsync(string url, int size, int numOfStartingLights)
        {
            return GetAsync<Matrix>(
                url: url,
                path: $"/setup/initial-matrix/?size={size}&numOfStartingLights={numOfStartingLights}",
                nameOfApi: "Setup API");
        }
    }
}
