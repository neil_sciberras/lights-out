﻿using System.Threading.Tasks;

using lightsout.application.clients.interfaces;
using lightsout.dto;

namespace lightsout.application.clients
{
    class SettingsClient : BaseClient, ISettingsClient
    {
        public Task<Settings> GetMatrixSettingsAsync(string url)
        {
            return GetAsync<Settings>(
                url: url, 
                path: "/settings", 
                nameOfApi: "Settings API");
        }
    }
}
