﻿using System.Diagnostics.CodeAnalysis;

namespace lightsout.application.configuration
{
    [ExcludeFromCodeCoverage]
    public class ApiOptions
    {
        public string Url { get; set; }

        public ApiOptions(string url)
        {
            Url = url;
        }
    }
}
