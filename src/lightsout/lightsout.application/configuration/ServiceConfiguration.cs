﻿using lightsout.application.clients;
using lightsout.application.clients.interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace lightsout.application.configuration
{
    public static class ServiceConfiguration
    {
        public static ServiceProvider Configure()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            return new ServiceCollection()
                .AddSingleton(_ => new ApiOptions(config["Api:Url"]))
                .AddSingleton<ISettingsClient, SettingsClient>()
                .AddSingleton<ISetupClient, SetupClient>()
                .AddAutoMapper(mapperConfig => mapperConfig.AddProfile(new AutoMapperProfile()))
                .BuildServiceProvider();
        }
    }
}
