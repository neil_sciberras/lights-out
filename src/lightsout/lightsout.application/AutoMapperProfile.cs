﻿using AutoMapper;

using lightsout.dto;

namespace lightsout.application
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Matrix, entities.Matrix>();
        }
    }
}
