﻿using System;
using System.Linq;

using lightsout.application.exceptions;

namespace lightsout.application.extensions
{
    public static class StringExtensions
    {
        public static (int, int) Parse(this string command)
        {
            var indices = command
                .RemoveAllWhiteSpaces()
                .Split(',');

            if (indices == null || indices.Length != 2)
            {
                throw new InvalidCommandException("Command did not have two indices.");
            }

            if (!(int.TryParse(indices[0], out var row) && int.TryParse(indices[1], out var column)))
            {
                throw new InvalidCommandException("Indices could not be parsed into integers.");
            }

            return new ValueTuple<int, int>(row, column);
        }

        public static bool IsQuitCommand(this string command)
        {
            return string.Equals(command.RemoveAllWhiteSpaces(), "q", StringComparison.InvariantCultureIgnoreCase);
        }

        private static string RemoveAllWhiteSpaces(this string command)
        {
            return string.Concat(command.Where(c => !char.IsWhiteSpace(c)));
        }
    }
}
