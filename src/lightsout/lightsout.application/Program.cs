﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

using AutoMapper;

using lightsout.application.clients.interfaces;
using lightsout.application.configuration;
using lightsout.application.exceptions;
using lightsout.application.extensions;
using lightsout.entities;
using lightsout.utilities;

using Microsoft.Extensions.DependencyInjection;

namespace lightsout.application
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                var serviceProvider = ServiceConfiguration.Configure();
                var matrix = await GetMatrixAsync(serviceProvider);

                Console.WriteLine("Welcome to LightsOut game, where you need to switch all the lights off." +
                                  "\nEnter commands of the form X,Y where X = row number and Y = column number (zero-based)." +
                                  "\nIn the matrix, O = On, - = Off." +
                                  "\n\nEnter q/Q if you give up :)");

                Console.WriteLine(matrix.GetStringRep());

                while (matrix.NumOfLitLights() != 0)
                {
                    Console.Write("\nEnter command: ");
                    var command = Console.ReadLine();

                    if (command.IsQuitCommand()) break;

                    (int, int) position;

                    try
                    {
                        position = command.Parse();
                        matrix = matrix.ToggleThisAndAdjacent(position);
                    }
                    catch (InvalidCommandException e)
                    {
                        Console.WriteLine($"\nInvalid command ({e.Message})");
                        continue;
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Console.WriteLine($"\nInvalid command (position was out of range)");
                        continue;
                    }

                    Console.WriteLine(matrix.GetStringRep());
                }

                if (matrix.NumOfLitLights() == 0)
                {
                    Console.WriteLine("Well done, all lights are switched off!");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Unhandled exception occurred. Aborting..");
                Console.WriteLine($"Exception: {e}");
            }

            Console.WriteLine("\nGoodbye!");
            Console.ReadLine();
        }

        private static async Task<Matrix> GetMatrixAsync(ServiceProvider serviceProvider)
        {
            var url = serviceProvider.GetService<ApiOptions>().NotNull(nameof(ApiOptions)).Url;
            var mapper = serviceProvider.GetService<IMapper>().NotNull("mapper");
            var settingsApi = serviceProvider.GetService<ISettingsClient>().NotNull(nameof(ISettingsClient));
            var setupApi = serviceProvider.GetService<ISetupClient>().NotNull(nameof(ISetupClient));

            var settingsDto = (await settingsApi.GetMatrixSettingsAsync(url)).NotNull("settingsDto");

            var matrixDto = await setupApi.GetInitialMatrixAsync(
                url: url,
                size: settingsDto.SquareMatrixSize,
                numOfStartingLights: settingsDto.NumOfStartingLights).NotNull("matrixDto");

            return mapper.Map<Matrix>(matrixDto);
        }
    }
}
