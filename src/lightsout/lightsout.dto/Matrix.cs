﻿using lightsout.utilities;

using Newtonsoft.Json;

namespace lightsout.dto
{
    public class Matrix
    {
        [JsonProperty]
        public bool[,] Lights { get; }

        [JsonConstructor]
        private Matrix(bool[,] lights)
        {
            Lights = lights;
        }

        public static Matrix Create(bool[,] lights)
        {
            return new Matrix(lights.NotNullOrEmpty(nameof(lights)));
        }
    }
}
