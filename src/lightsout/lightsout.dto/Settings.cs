﻿using lightsout.utilities;

using Newtonsoft.Json;

namespace lightsout.dto
{
    public class Settings
    {
        [JsonProperty]
        public int SquareMatrixSize { get; private set; }

        [JsonProperty]
        public int NumOfStartingLights { get; private set; }

        [JsonConstructor]
        private Settings(int squareMatrixSize, int numOfStartingLights)
        {
            SquareMatrixSize = squareMatrixSize;
            NumOfStartingLights = numOfStartingLights;
        }

        public static Settings Create(int squareMatrixSize, int numOfStartingLights)
        {
            return new Settings(
                squareMatrixSize: squareMatrixSize.NotNegativeOrZero(nameof(squareMatrixSize)),
                numOfStartingLights: numOfStartingLights
                    .NotNegativeOrZero(nameof(numOfStartingLights))
                    .NotGreaterThanTheSquareOf(
                        otherVal: squareMatrixSize,
                        argName: nameof(numOfStartingLights),
                        nameOfOtherVal: nameof(squareMatrixSize)));
        }
    }
}
