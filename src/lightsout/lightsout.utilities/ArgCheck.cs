﻿using System;

namespace lightsout.utilities
{
    public static class ArgCheck
    {
        public static T NotNull<T>(this T arg, string argName)
        {
            return arg ?? throw new ArgumentNullException(paramName: argName);
        }

        public static string NotNullOrEmpty(this string arg, string argName)
        {
            return !string.IsNullOrEmpty(arg)
                ? arg
                : throw new ArgumentException(paramName: argName, message: "Specified argument was null or empty.");
        }

        public static bool[,] NotNullOrEmpty(this bool[,] arg, string argName)
        {
            return arg != null && arg.Length > 0
                ? arg
                : throw new ArgumentException(paramName: argName, message: "Specified argument was null or empty.");
        }

        public static int NotNegativeOrZero(this int arg, string argName)
        {
            return arg > 0
                ? arg
                : throw new ArgumentOutOfRangeException(paramName: argName, message: "Specified argument was less than or equal to zero.");
        }

        public static int NotGreaterThanTheSquareOf(this int arg, int otherVal, string argName, string nameOfOtherVal)
        {
            return arg <= otherVal*otherVal
                ? arg
                : throw new ArgumentOutOfRangeException(paramName: argName, message: $"Specified argument was greater than the square of {nameOfOtherVal}.");
        }
    }
}
