﻿using System.Diagnostics.CodeAnalysis;

namespace lightsout.infrastructure.ioc
{
    [ExcludeFromCodeCoverage]
    public class ConnectionStringOptions
    {
        public string SettingsDb { get; set; }

        public ConnectionStringOptions(string settingsDb)
        {
            SettingsDb = settingsDb;
        }
    }
}
