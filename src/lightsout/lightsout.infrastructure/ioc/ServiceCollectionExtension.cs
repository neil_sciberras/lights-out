﻿using System.Diagnostics.CodeAnalysis;

using lightsout.infrastructure.repositories;
using lightsout.infrastructure.repositories.interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace lightsout.infrastructure.ioc
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddInfrastructureDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(_ => new ConnectionStringOptions(configuration.GetConnectionString("SettingsDb")));
            services.AddSingleton<ISettingsDbRepository, SettingsDbRepository>();

            return services;
        }
    }
}
