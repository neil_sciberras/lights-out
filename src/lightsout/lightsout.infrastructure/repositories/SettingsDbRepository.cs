﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

using lightsout.dto;
using lightsout.infrastructure.ioc;
using lightsout.infrastructure.repositories.interfaces;
using lightsout.utilities;

namespace lightsout.infrastructure.repositories
{
    public class SettingsDbRepository : ISettingsDbRepository
    {
        private readonly ConnectionStringOptions _connectionStringOptions;

        public SettingsDbRepository(ConnectionStringOptions connectionStringOptions)
        {
            _connectionStringOptions = connectionStringOptions.NotNull(nameof(connectionStringOptions));
        }

        public async Task<Settings> GetSettingsAsync()
        {
            try
            {
                await using var connection = await GetConnectionAsync();
                var reader = await GetReaderAsync(connection: connection, spName: "spGetLatestSettings");
                return await ReadAndConvertAsync(reader);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error reading from the DB. Exception: {e}");
                throw;
            }
        }

        private async Task<SqlConnection> GetConnectionAsync()
        {
            var connection = new SqlConnection(_connectionStringOptions.SettingsDb);
            await connection.OpenAsync();
            return connection;
        }

        private async Task<SqlDataReader> GetReaderAsync(SqlConnection connection, string spName)
        {
            var command = new SqlCommand(cmdText: spName, connection: connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            return await command.ExecuteReaderAsync();
        }

        private async Task<Settings> ReadAndConvertAsync(SqlDataReader reader)
        {
            await reader.ReadAsync();

            return Settings.Create(
                squareMatrixSize: reader.GetInt32(reader.GetOrdinal("SquareMatrixSize")),
                numOfStartingLights: reader.GetInt32(reader.GetOrdinal("NumOfStartingLights")));
        }
    }
}
