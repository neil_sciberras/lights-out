﻿using System.Threading.Tasks;

using lightsout.dto;

namespace lightsout.infrastructure.repositories.interfaces
{
    public interface ISettingsDbRepository
    {
        public Task<Settings> GetSettingsAsync();
    }
}
