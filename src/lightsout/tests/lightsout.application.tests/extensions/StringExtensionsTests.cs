﻿using lightsout.application.exceptions;
using lightsout.application.extensions;

using NUnit.Framework;

namespace lightsout.application.tests.extensions
{
    public class StringExtensionsTests
    {
        [TestCase("", "Command did not have two indices.")]
        [TestCase("1,2,3", "Command did not have two indices.")]
        [TestCase("1 2", "Command did not have two indices.")]
        [TestCase("dummy", "Command did not have two indices.")]
        [TestCase("1,d", "Indices could not be parsed into integers.")]
        [TestCase("1,2.0", "Indices could not be parsed into integers.")]
        public void GivenInvalidCommand_ThenParseThrows(string invalidCommand, string expectedExceptionMessage)
        {
            // Act
            var e = Assert.Throws<InvalidCommandException>(() => invalidCommand.Parse());

            // Assert
            Assert.NotNull(e);
            Assert.AreEqual(expectedExceptionMessage, e.Message);
        }

        [TestCase("    1,   2    ")]
        [TestCase("1,2")]
        public void GivenValidCommand_ThenParseReturnsCorrectTuple(string validCommand)
        {
            // Act
            var tuple = validCommand.Parse();

            // Assert
            Assert.NotNull(tuple);
            Assert.AreEqual(1, tuple.Item1);
            Assert.AreEqual(2, tuple.Item2);
        }

        [TestCase("q", true)]
        [TestCase("Q", true)]
        [TestCase(" Q  ", true)]
        [TestCase("Qdummy", false)]
        [TestCase("a", false)]
        public void IsQuitCommandTests(string command, bool expectedResult)
        {
            // Assert
            Assert.AreEqual(expectedResult, command.IsQuitCommand());
        }
    }
}
