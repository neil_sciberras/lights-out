﻿using lightsout.entities.extensions;

using NUnit.Framework;

namespace lightsout.entities.tests
{
    public class MatrixTests
    {
        private readonly Matrix _matrixExample = new Matrix(new bool[,]
        {
            {false, false, false, true, true},
            {false, false, false, true, false},
            {false, false, false, false, false},
            {false, false, false, false, false},
            {false, false, false, false, false}
        });

        [Test]
        public void NumOfLitLightsReturnsCorrectly()
        {
            // Act
            // Assert
            Assert.AreEqual(3, _matrixExample.NumOfLitLights());
        }

        [Test]
        public void ToggleThisAndAdjacentTest()
        {
            // Act
            var positionToToggle = (1, 3);
            var newMatrix = _matrixExample.ToggleThisAndAdjacent(positionToToggle);

            // Assert
            for (int i = 0; i < _matrixExample.Lights.GetLength(0); i++)
            {
                for (int j = 0; j < _matrixExample.Lights.GetLength(1); j++)
                {
                    if ((i, j) == positionToToggle
                        || (i, j) == positionToToggle.OneUp()
                        || (i, j) == positionToToggle.OneDown()
                        || (i, j) == positionToToggle.OneLeft()
                        || (i, j) == positionToToggle.OneRight())
                    {
                        Assert.AreNotEqual(_matrixExample.Lights[i, j], newMatrix.Lights[i, j]);
                    }
                    else
                    {
                        Assert.AreEqual(_matrixExample.Lights[i, j], newMatrix.Lights[i, j]);
                    }
                }
            }
        }
    }
}
