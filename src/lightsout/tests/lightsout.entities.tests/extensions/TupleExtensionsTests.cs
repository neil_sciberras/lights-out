﻿using lightsout.entities.extensions;

using NUnit.Framework;

namespace lightsout.entities.tests.extensions
{
    public class TupleExtensionsTests
    {
        private (int, int) _position = (2, 3);

        [TestCase(2, 3, true)]
        [TestCase(1, 4, true)]
        [TestCase(3, 4, false)]
        [TestCase(3, 1, false)]
        public void BetweenTests(int lowerLimit, int upperLimit, bool expectedResult)
        {
            // Act
            var result = _position.Between(lowerLimit, upperLimit);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void OneUpTest()
        {
            // Act
            var returnedPosition = _position.OneUp();

            // Assert
            Assert.NotNull(returnedPosition);
            Assert.AreEqual(_position.Item1 - 1, returnedPosition.Item1);
            Assert.AreEqual(_position.Item2, returnedPosition.Item2);
        }

        [Test]
        public void OneDownTest()
        {
            // Act
            var returnedPosition = _position.OneDown();

            // Assert
            Assert.NotNull(returnedPosition);
            Assert.AreEqual(_position.Item1 + 1, returnedPosition.Item1);
            Assert.AreEqual(_position.Item2, returnedPosition.Item2);
        }

        [Test]
        public void OneLeftTest()
        {
            // Act
            var returnedPosition = _position.OneLeft();

            // Assert
            Assert.NotNull(returnedPosition);
            Assert.AreEqual(_position.Item1, returnedPosition.Item1);
            Assert.AreEqual(_position.Item2 - 1, returnedPosition.Item2);
        }

        [Test]
        public void OneRightTest()
        {
            // Act
            var returnedPosition = _position.OneRight();

            // Assert
            Assert.NotNull(returnedPosition);
            Assert.AreEqual(_position.Item1, returnedPosition.Item1);
            Assert.AreEqual(_position.Item2 + 1, returnedPosition.Item2);
        }
    }
}
