﻿using System;
using System.Threading.Tasks;

using lightsout.dto;
using lightsout.infrastructure.repositories.interfaces;

using Moq;

using NUnit.Framework;

namespace lightsout.services.tests
{
    public class SettingsServiceTests
    {
        private SettingsService _sut;
        private Mock<ISettingsDbRepository> _settingsDbRepo;

        [SetUp]
        public void Setup()
        {
            _settingsDbRepo = new Mock<ISettingsDbRepository>();
        }

        [Test]
        public async Task WhenRepoReturnsCorrectly_ThenServiceReturnsCorrectly()
        {
            // Arrange
            var expectedSquareMatrixSize = 5;
            var expectedNumOfStartingLights = 2;

            _settingsDbRepo
                .Setup(r => r.GetSettingsAsync())
                .ReturnsAsync(Settings.Create(expectedSquareMatrixSize, expectedNumOfStartingLights));

            _sut = new SettingsService(_settingsDbRepo.Object);

            // Act
            var result = await _sut.GetSettingsAsync();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(expectedSquareMatrixSize, result.SquareMatrixSize);
            Assert.AreEqual(expectedNumOfStartingLights, result.NumOfStartingLights);
        }

        [Test]
        public void WhenRepoThrows_ThenServiceThrows()
        {
            // Arrange
            _settingsDbRepo.Setup(r => r.GetSettingsAsync()).ThrowsAsync(new Exception());
            _sut = new SettingsService(_settingsDbRepo.Object);

            // Act
            // Assert
            Assert.ThrowsAsync<Exception>(() => _sut.GetSettingsAsync());
        }
    }
}
