﻿using System.Threading.Tasks;

using lightsout.dto;

using NUnit.Framework;

namespace lightsout.services.tests
{
    public class SetupServiceTests
    {
        [Test]
        public async Task GetInitialMatrixReturnsTheExpectedAmountOfStartingLights()
        {
            // Arrange
            var expectedAmountOfStartingLights = 25;
            var _sut = new SetupService();

            // Act
            var matrix = await _sut.GetInitialMatrixAsync(size: 5, numOfStartingLights: expectedAmountOfStartingLights);

            // Assert
            Assert.AreEqual(expectedAmountOfStartingLights, NumOfLitLights(matrix));
        }

        private int NumOfLitLights(Matrix matrix)
        {
            var counter = 0;

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if (matrix.Lights[i, j]) counter++;
                }
            }

            return counter;
        }
    }
}
