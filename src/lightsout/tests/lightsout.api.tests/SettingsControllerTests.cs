﻿using System;
using System.Threading.Tasks;

using lightsout.api.Controllers;
using lightsout.dto;
using lightsout.services.interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using NUnit.Framework;

namespace lightsout.api.tests
{
    public class SettingsControllerTests
    {
        private SettingsController _sut;
        private Mock<ISettingsService> _settingsServiceMock;

        [SetUp]
        public void Setup()
        {
            _settingsServiceMock = new Mock<ISettingsService>();
        }

        [Test]
        public async Task WhenServiceReturnsCorrectly_ThenGetAsyncReturns200AndSettings()
        {
            // Arrange
            _settingsServiceMock
                .Setup(s => s.GetSettingsAsync())
                .ReturnsAsync(Settings.Create(5, 2));

            _sut = new SettingsController(_settingsServiceMock.Object);

            // Act
            var result = (OkObjectResult)await _sut.GetAsync();

            // Assert
            Assert.NotNull(result?.StatusCode);
            Assert.AreEqual(StatusCodes.Status200OK, result.StatusCode.Value);
            Assert.NotNull((Settings)result.Value);
            _settingsServiceMock.Verify(m => m.GetSettingsAsync(), Times.Once);
        }

        [Test]
        public async Task WhenServiceThrows_ThenGetAsyncReturns500()
        {
            // Arrange
            _settingsServiceMock.Setup(s => s.GetSettingsAsync()).ThrowsAsync(new Exception());
            _sut = new SettingsController(_settingsServiceMock.Object);

            // Act
            var result = (StatusCodeResult)await _sut.GetAsync();

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(StatusCodes.Status500InternalServerError, result.StatusCode);
        }
    }
}
