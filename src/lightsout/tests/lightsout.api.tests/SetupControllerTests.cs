﻿using System;
using System.Threading.Tasks;

using lightsout.api.Controllers;
using lightsout.dto;
using lightsout.services.interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Moq;

using NUnit.Framework;

namespace lightsout.api.tests
{
    public class SetupControllerTests
    {
        private SetupController _sut;
        private Mock<ISetupService> _setupServiceMock;

        [SetUp]
        public void Setup()
        {
            _setupServiceMock = new Mock<ISetupService>();
        }

        [Test]
        public async Task WhenServiceReturnsCorrectly_ThenGetReturns200AndMatrix()
        {
            // Arrange
            _setupServiceMock
                .Setup(m => m.GetInitialMatrixAsync(5, 3))
                .ReturnsAsync(Matrix.Create(new bool[5,5]));

            _sut = new SetupController(_setupServiceMock.Object);

            // Act
            var result = (OkObjectResult)await _sut.GetInitialMatrixAsync(size: 5, numOfStartingLights: 3);

            // Assert
            Assert.NotNull(result?.StatusCode);
            Assert.AreEqual(StatusCodes.Status200OK, result.StatusCode);
            Assert.NotNull((Matrix)result.Value);
            _setupServiceMock.Verify(m => m.GetInitialMatrixAsync(5, 3), Times.Once);
        }

        [Test]
        public async Task WhenServiceThrows_ThenGetAsyncReturns500()
        {
            // Arrange
            _setupServiceMock.Setup(s => s.GetInitialMatrixAsync(It.IsAny<int>(), It.IsAny<int>())).ThrowsAsync(new ArgumentOutOfRangeException());
            _sut = new SetupController(_setupServiceMock.Object);

            // Act
            var result = (StatusCodeResult)await _sut.GetInitialMatrixAsync(5, 3);

            // Assert
            Assert.NotNull(result);
            Assert.AreEqual(StatusCodes.Status500InternalServerError, result.StatusCode);
        }
    }
}
