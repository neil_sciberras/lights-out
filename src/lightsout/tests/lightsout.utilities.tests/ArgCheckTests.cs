using System;

using NUnit.Framework;

namespace lightsout.utilities.tests
{
    public class ArgCheckTests
    {
        #region NotNull

        [Test]
        public void GivenNullArg_ThenNotNullThrowsThrows()
        {
            // Arrange
            // Act
            var arg = (int?)null;
            var e = Assert.Throws<ArgumentNullException>(() => arg.NotNull(nameof(arg)));

            // Assert
            Assert.AreEqual("Value cannot be null. (Parameter 'arg')", e.Message);
        }

        [Test]
        public void GivenNotNullArg_ThenNotNullReturns()
        {
            // Arrange
            // Act
            var arg = 1;
            var returnedValue = arg.NotNull(nameof(arg));

            // Assert
            Assert.AreEqual(arg, returnedValue);
        }

        #endregion

        #region NotNegativeOrZero

        [TestCase(0)]
        [TestCase(-1)]
        public void GivenNegativeOrZero_ThenNotNegativeOrZeroThrows(int arg)
        {
            // Act
            var e = Assert.Throws<ArgumentOutOfRangeException>(() => arg.NotNegativeOrZero(nameof(arg)));

            // Assert
            Assert.AreEqual("Specified argument was less than or equal to zero. (Parameter 'arg')", e.Message);
        }

        [Test]
        public void GivenPositive_ThenNotNegativeOrZeroReturns()
        {
            // Arrange
            var arg = 1;

            // Act
            var returnedValue = arg.NotNegativeOrZero(nameof(arg));

            // Assert
            Assert.AreEqual(arg, returnedValue);
        }

        #endregion

        #region NotNullOrEmpty

        [TestCase("")]
        [TestCase(null)]
        public void GivenNullOrEmpty_ThenNotNullOrEmptyThrows(string arg)
        {
            // Act
            var e = Assert.Throws<ArgumentException>(() => arg.NotNullOrEmpty(nameof(arg)));

            // Assert
            Assert.AreEqual("Specified argument was null or empty. (Parameter 'arg')", e.Message);
        }

        [Test]
        public void GivenValidString_ThenNotNullOrEmptyReturns()
        {
            // Arrange
            var arg = "dummy";

            // Act
            var returnedValue = arg.NotNullOrEmpty(nameof(arg));

            // Assert
            Assert.AreEqual(arg, returnedValue);
        }

        #endregion

        #region NotGreaterThanTheSquareOf

        [Test]
        public void GivenValueGreaterThanExpected_ThenNotGreaterThanTheSquareOfThrows()
        {
            // Arrange
            // Act
            var e = Assert.Throws<ArgumentOutOfRangeException>(() => 26.NotGreaterThanTheSquareOf(otherVal: 5, argName: "26", nameOfOtherVal: "5"));
            
            // Assert
            Assert.NotNull(e);
            Assert.AreEqual("Specified argument was greater than the square of 5. (Parameter '26')", e.Message);
        }

        [Test]
        public void GivenValueWithinExpectedRange_ThenNotGreaterThanTheSquareOfReturns()
        {
            // Arrange
            // Act
            var returnedValue = 25.NotGreaterThanTheSquareOf(otherVal: 5, argName: "25", nameOfOtherVal: "5");

            // Assert
            Assert.NotNull(returnedValue);
            Assert.AreEqual(25, returnedValue);
        }

        #endregion
    }
}