﻿using System;

using lightsout.dto;

using NUnit.Framework;

namespace lightsout.models.tests
{
    public class SettingsTests
    {
        [TestCase(-1, 1)]
        [TestCase(0, 1)]
        [TestCase(5, -1)]
        [TestCase(5, 0)]
        [TestCase(5, 26)]
        public void GivenInvalidValues_ThenCreateThrows(int squareMatrixSize, int numOfStartingLights)
        {
            // Arrange
            // Act
            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => Settings.Create(squareMatrixSize, numOfStartingLights));
        }

        [Test]
        public void GivenValidValue_ThenSettingsCreatedCorrectly()
        {
            // Arrange
            var expectedSquareMatrixSize = 5;
            var expectedNumOfStartingLights = 2;

            // Act
            var settings = Settings.Create(expectedSquareMatrixSize, expectedNumOfStartingLights);

            // Assert
            Assert.NotNull(settings);
            Assert.AreEqual(expectedSquareMatrixSize, settings.SquareMatrixSize);
            Assert.AreEqual(expectedNumOfStartingLights, settings.NumOfStartingLights);
        }
    }
}
