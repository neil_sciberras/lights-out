using System;

using lightsout.infrastructure.ioc;
using lightsout.infrastructure.repositories;

using NUnit.Framework;

namespace lightsout.infrastructure.tests
{
    public class SettingsDbRepositoryTests
    {
        [Test]
        public void GivenInvalidConnectionString_ThenGetSettingsAsyncThrows()
        {
            // Arrange
            var invalidConnectionStringOptions = new ConnectionStringOptions("dummy");
            var sut = new SettingsDbRepository(invalidConnectionStringOptions);

            // Act
            var e = Assert.ThrowsAsync<ArgumentException>(() => sut.GetSettingsAsync());

            // Assert
            Assert.AreEqual("Format of the initialization string does not conform to specification starting at index 0.", e.Message);
        }
    }
}