﻿using System.Threading.Tasks;

using lightsout.dto;

namespace lightsout.services.interfaces
{
    public interface ISetupService
    {
        public Task<Matrix> GetInitialMatrixAsync(int size, int numOfStartingLights);
    }
}
