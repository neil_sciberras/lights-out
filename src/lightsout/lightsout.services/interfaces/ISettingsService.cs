﻿using System.Threading.Tasks;

using lightsout.dto;

namespace lightsout.services.interfaces
{
    public interface ISettingsService
    {
        public Task<Settings> GetSettingsAsync();
    }
}
