﻿using System.Diagnostics.CodeAnalysis;

using lightsout.infrastructure.ioc;
using lightsout.services.interfaces;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace lightsout.services.ioc
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddServicesDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddInfrastructureDependencies(configuration);

            services.AddSingleton<ISettingsService, SettingsService>();
            services.AddSingleton<ISetupService, SetupService>();

            return services;
        }
    }
}