﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using lightsout.dto;
using lightsout.services.interfaces;

namespace lightsout.services
{
    public class SetupService : ISetupService
    {
        public async Task<Matrix> GetInitialMatrixAsync(int size, int numOfStartingLights)
        {
            var randomPositions = await Task.Run(() =>
                GetRandomPositions(amount: numOfStartingLights, maxSize: size));

            var lights = new bool[size, size];

            randomPositions.ToList().ForEach(pos => lights[pos.Item1, pos.Item2] = true);

            return Matrix.Create(lights);
        }

        private IEnumerable<(int, int)> GetRandomPositions(int amount, int maxSize)
        {
            var random = new Random();
            var tuples = new List<(int, int)>();

            for (var i = 0; i < amount; i++)
            {
                var tuple = (random.Next(maxSize), random.Next(maxSize));

                while (tuples.Any(t => t.Item1 == tuple.Item1 && t.Item2 == tuple.Item2))
                {
                    tuple = (random.Next(maxSize), random.Next(maxSize));
                }

                tuples.Add(tuple);
            }

            return tuples;
        }
    }
}
