﻿using System.Threading.Tasks;

using lightsout.dto;
using lightsout.infrastructure.repositories.interfaces;
using lightsout.services.interfaces;

namespace lightsout.services
{
    public class SettingsService : ISettingsService
    {
        private readonly ISettingsDbRepository _settingsDbRepository;

        public SettingsService(ISettingsDbRepository settingsDbRepository)
        {
            _settingsDbRepository = settingsDbRepository;
        }

        public Task<Settings> GetSettingsAsync()
        {
            return _settingsDbRepository.GetSettingsAsync();
        }
    }
}
